
<div class="fl-logo-slider">
	<div class="fr-slider" data-fr='{"slidesToScroll":6,"slidesToShow":6,"arrows":false,"infinite": false}'>
		<a href="#" class="arrow prev"><i class="fa fa-angle-left"></i></a>
    	<a href="#" class="arrow next"><i class="fa fa-angle-right"></i></a>
		<div class="slides">
			<?php foreach($settings->photos as $id){ ?>
				<div class="slide"><?php echo wp_get_attachment_image($id,"logo_slider") ?></div>
			<?php } ?>
	  	</div>
	</div>
	<div class="fl-post-grid-sizer"></div>
</div>
<div class="fl-clear"></div>
